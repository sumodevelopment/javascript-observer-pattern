const express = require('express')
const app = express()
const path = require('path')
const { PORT = 3000 } = process.env
const db = require('./db-config')

// Middleware
app.use(express.json())
app.use(express.static(path.join(__dirname, 'client')))

// Routing
app.use('/', require('./routes'))
app.use('/api', require('./api-routes'))

// Startup
app.listen(PORT, () => console.log(`Server running on port ${PORT}...`))

module.exports = app