import State from './core/State.js'
import PersonList from './components/PersonList.js'
import PersonCount from './components/PersonCount.js'
import Loading from './components/Loading.js'

class App {

    components;
    state = new State()

    constructor(components = []) {
        this.components = components
        for (const component of this.components) {
            this.state.subscribe(component)
        }
    }

    addComponent(component) {
        this.state.subscribe(component)
    }

    init() {

        this.state.update({ status: 'Loading...' })
    
        fetch('https://randomuser.me/api/?results=20')
            .then(r => r.json())
            .then(response => response.results)
            .then(people => {
                this.state.update({ people, status: '' })
            })

        return this
    }

    render() {
        for (const component of this.components) {
            component.render(this.state.get())
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    new App([
        new Loading('loading-status'),
        new PersonList('person-list'),
        new PersonCount('person-count')
    ]).init().render()
})


/**
 * Inspired by tutorial from:
 * https://webdevstudios.com/2019/02/19/observable-pattern-in-javascript/#:~:text=The%20observer%20pattern%20is%20a,one%2Dto%2Dmany%20relationship.
 */