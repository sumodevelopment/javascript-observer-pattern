import Subject from './Subject.js'

class State extends Subject {
    state = {}
    
    constructor() {
        super()
    }

    update(data = {}) {
        this.state = Object.assign(this.state, data)
        this.next(this.state)
    }

    get() {
        return this.state
    }
}

export default State