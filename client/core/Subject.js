class Subject {
    observers = []

    subscribe(observer) {
        this.observers.push( observer )
    }

    unsubscribe( observer ) {
        this.observers = this.filter(obs => obs !== observer)
    }

    next(data) {
        if (this.observers.length === 0) {
            return false
        }

        for (const observer of this.observers) {
            observer.update(data)
        }
    }
}

export default Subject