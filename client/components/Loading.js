import Observer from '../core/Observer.js'

class Loading extends Observer {
    root;
    constructor(selector) {
        super()
        this.root = document.getElementById(selector)
    }

    html({ status = '' }) {
        console.log(status);
        return `<p>${status}</p>`
    }

    update(state) {
        this.render(state)
    }

    render(state) {
        this.root.innerHTML = this.html(state)
    }
}

export default Loading