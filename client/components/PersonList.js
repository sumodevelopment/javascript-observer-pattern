import Observer from '../core/Observer.js'
class PersonList extends Observer {

    root;

    constructor(selector) {
        super()
        this.root = document.getElementById(selector)
    }

    update(state) {
        this.render(state)
    }

    html({ people = [] }) {
        const list = people.map(person => `
            <li class="person">
                <div class="person__avatar">
                    <img class="person__avatar-image" src="${person.picture.thumbnail}" alt="${person.name.first} ${person.name.last}"/>
                </div>
                <div class="person__meta">
                    <h4>${person.name.first} ${person.name.last}</h4>
                    <p class="col-grey">${person.email}</p>
                    <p class="col-grey">${person.phone}</p>
                <div>
            </li>
        `).join('')
        return `<ul>${ list }</ul>`
    }

    render(state) {
        this.root.innerHTML = this.html(state)
    }

}

export default PersonList