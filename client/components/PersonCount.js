import Observer from '../core/Observer.js'

class PersonCount extends Observer {
    
    root;

    constructor(selector) {
        super()
        this.root = document.getElementById(selector)
    }

    html({ people = [] }) {
        return `<p>People: ${ people.length }</p>`
    }

    render(state) {
        this.root.innerHTML = this.html(state)
    }

    update(state) {
        this.render(state)
    }
}

export default PersonCount