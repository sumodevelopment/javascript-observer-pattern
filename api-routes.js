const router = require('express').Router()
const path = require('path')
const db = require('./db-config')

router.get('/books', (req, res) => { 
    const books = db.get('books')
    setTimeout(() =>{
        return res.json(books)
    }, 3000)   
})

module.exports = router